import logging
import os

import pytest

from prtg_pyprobe.utils.logging import setup_logging, setup_file_logging


class TestLogging:
    def test_setup_logging(self):
        logger, console_handler, formatter = setup_logging()
        assert type(logger) == logging.RootLogger
        assert type(console_handler) == logging.StreamHandler
        assert type(formatter) == logging.Formatter
        assert logger.hasHandlers()
        assert logger.name == "root"
        assert console_handler.formatter is formatter

    def test_setup_file_logging(self, probe_config_dict):
        logger, console_handler, formatter = setup_logging()
        file_logger = setup_file_logging(probe_config_dict, logger, formatter, console_handler)
        assert type(file_logger) == logging.RootLogger
        assert os.path.exists(probe_config_dict["log_file_location"])
        file_logger.disabled = True
        os.unlink(probe_config_dict["log_file_location"])

    def test_setup_file_logging_exception(self, probe_config_dict):
        logger, console_handler, formatter = setup_logging()
        probe_config_dict["log_level"] = "NON_EXISTENT_LOG_LEVEL"
        with pytest.raises(Exception):
            assert setup_file_logging(probe_config_dict, logger, formatter, console_handler)

import asyncio

import pytest
from pysnmp.error import PySnmpError
from pysnmp.hlapi.asyncio import SnmpEngine
from pysnmp.smi.error import NoSuchObjectError

from prtg_pyprobe.sensors import sensor_snmp_cpuload as module
from prtg_pyprobe.sensors.sensor import SensorSNMPBase


async def snmp_get_success(*args, **kwargs):
    return [
        (".1.3.6.1.4.1.2021.10.1.3.1", 1234),
        (".1.3.6.1.4.1.2021.10.1.3.2", 5678),
        (".1.3.6.1.4.1.2021.10.1.3.3", 9123),
    ]


async def snmp_get_empty_value(*args, **kwargs):
    return [
        (".1.3.6.1.4.1.2021.10.1.3.1", ""),
        (".1.3.6.1.4.1.2021.10.1.3.2", ""),
        (".1.3.6.1.4.1.2021.10.1.3.3", ""),
    ]


async def snmp_get_pysnmp_error(*args, **kwargs):
    raise PySnmpError


async def snmp_get_runtime_error(*args, **kwargs):
    raise RuntimeError


async def snmp_get_value_error(*args, **kwargs):
    raise ValueError


async def snmp_get_no_such_object_error(*args, **kwargs):
    raise NoSuchObjectError


class TestSNMPCPUSensorProperties:
    def test_sensor_snmp_cpu_load_base_class(self, snmp_cpu_load_sensor):
        assert type(snmp_cpu_load_sensor).__bases__[0] is SensorSNMPBase

    def test_sensor_snmp_custom_name(self, snmp_cpu_load_sensor):
        assert snmp_cpu_load_sensor.name == "SNMP CPU Load Avg"

    def test_sensor_snmp_custom_kind(self, snmp_cpu_load_sensor):
        assert snmp_cpu_load_sensor.kind == "mpsnmpcpuloadavg"

    def test_sensor_snmp_custom_definition(self, snmp_cpu_load_sensor):
        assert snmp_cpu_load_sensor.definition == {
            "description": "Monitors Load Avg via SNMP.",
            "groups": [
                {
                    "caption": "SNMP Specific",
                    "fields": [
                        {
                            "caption": "Community String",
                            "help": "Please enter the community string.",
                            "name": "community",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "SNMP Port",
                            "help": "Please enter SNMP Port.",
                            "name": "port",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "SNMP Version",
                            "default": "1",
                            "help": "Choose your SNMP Version",
                            "name": "snmp_version",
                            "options": {"0": "V1", "1": "V2c", "2": "V3"},
                            "required": "1",
                            "type": "radio",
                        },
                    ],
                    "name": "snmpspecific",
                }
            ],
            "help": "This sensor monitors the load avg of a given target via SNMP.",
            "kind": "mpsnmpcpuloadavg",
            "name": "SNMP CPU Load Avg",
            "tag": "mpsnmpcpuloadavg",
        }


@pytest.mark.asyncio
class TestSNMPCPUSensorWork:
    async def test_sensor_snmp_cpuload_success(self, snmp_cpu_load_sensor, snmp_cpuload_sensor_taskdata, monkeypatch):
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_success)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_cpu_load_sensor.work(task_data=snmp_cpuload_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        assert ret["sensorid"] == "1234"
        assert ret["message"] == "OK"
        assert {
            "name": "Load Avg 1 min",
            "mode": "float",
            "value": 1234,
            "unit": "Custom",
            "customunit": "",
        } in ret["channel"]
        assert {
            "name": "Load Avg 5min",
            "mode": "float",
            "value": 5678,
            "unit": "Custom",
            "customunit": "",
        } in ret["channel"]
        assert {
            "name": "Load Avg 10 min",
            "mode": "float",
            "value": 9123,
            "unit": "Custom",
            "customunit": "",
        } in ret["channel"]

    async def test_sensor_snmp_cpuload_work_empty_value(
        self,
        mocker,
        snmp_cpu_load_sensor,
        snmp_custom_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_empty_value)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_cpu_load_sensor.work(task_data=snmp_custom_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "SNMP Query failed. See logs for details"
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been an error in PySNMP.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_snmp_cpuload_work_pysnmp_error(
        self,
        mocker,
        snmp_cpu_load_sensor,
        snmp_cpuload_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_pysnmp_error)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_cpu_load_sensor.work(task_data=snmp_cpuload_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "SNMP Query failed. See logs for details"
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been an error in PySNMP.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_snmp_cpuload_work_runtime_error(
        self,
        mocker,
        snmp_cpu_load_sensor,
        snmp_cpuload_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_runtime_error)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_cpu_load_sensor.work(task_data=snmp_cpuload_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "SNMP Query failed. See logs for details"
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been an error.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_snmp_cpuload_work_value_error(
        self,
        mocker,
        snmp_cpu_load_sensor,
        snmp_cpuload_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_value_error)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_cpu_load_sensor.work(task_data=snmp_cpuload_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "The SNMP Query did return a not supported value type."
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("The SNMP query probably returned an unsupported value.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_snmp_cpuload_no_such_object_error(
        self,
        mocker,
        snmp_cpu_load_sensor,
        snmp_cpuload_sensor_taskdata,
        monkeypatch,
        sensor_exception_message,
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(SensorSNMPBase, "get", snmp_get_no_such_object_error)
        snmp_engine = SnmpEngine()
        test_q = asyncio.Queue()
        await snmp_cpu_load_sensor.work(task_data=snmp_cpuload_sensor_taskdata, q=test_q, snmp_engine=snmp_engine)
        ret = await test_q.get()
        sensor_exception_message["message"] = "The OID is wrong."
        assert ret == sensor_exception_message
        logging_mock.exception.assert_called_with("No such object. Probably the given OID is wrong.")
        logging_mock.exception.assert_called_once()

import asyncio
import collections
from datetime import timedelta

import pytest
from httpx import AsyncClient as Aclient

from prtg_pyprobe.sensors.sensor import SensorBase


async def ext_ip_sensor_response(*args, **kwargs):
    httpx_aclient_response_mock = collections.namedtuple("response", "elapsed text")
    return httpx_aclient_response_mock(elapsed=timedelta(microseconds=2000), text="1.3.3.7")


def task_data_ext_ip():
    return {
        "sensorid": "1234",
        "timeout": 30,
    }


class TestExternalIPSensorProperties:
    def test_sensor_ext_ip_base_class(self, external_ip_sensor):
        assert type(external_ip_sensor).__bases__[0] is SensorBase

    def test_sensor_ext_ip_name(self, external_ip_sensor):
        assert external_ip_sensor.name == "External IP (Probe)"

    def test_sensor_ext_ip_kind(self, external_ip_sensor):
        assert external_ip_sensor.kind == "mpexternalip"

    def test_sensor_ext_ip_definition(self, external_ip_sensor):
        assert external_ip_sensor.definition == {
            "description": "Returns the external ip address of the probe.",
            "groups": [
                {
                    "caption": "External IP Settings",
                    "fields": [
                        {
                            "caption": "Timeout (in s)",
                            "default": 30,
                            "help": "If the reply takes longer than this value "
                            "the request is aborted and an error message "
                            "is triggered. Maximum value is 300 sec. (= "
                            "5.0 minutes)",
                            "maximum": 300,
                            "minimum": 10,
                            "name": "timeout",
                            "required": "1",
                            "type": "integer",
                        }
                    ],
                    "name": "extipsettings",
                }
            ],
            "help": "Returns the external ip address of the probe using the website "
            "checkip.amazonaws.com.This is always the case even if added to a "
            "different device than the Probe Device.",
            "kind": "mpexternalip",
            "name": "External IP (Probe)",
            "tag": "mpexternalipsensor",
        }


@pytest.mark.asyncio
class TestExternalIPSensorWork:
    async def test_sensor_ext_ip_work(self, external_ip_sensor, monkeypatch):
        monkeypatch.setattr(Aclient, "send", ext_ip_sensor_response)
        ext_ip_queue = asyncio.Queue()
        await external_ip_sensor.work(task_data=task_data_ext_ip(), q=ext_ip_queue)
        ext_ip_out = await ext_ip_queue.get()
        assert ext_ip_out["message"] == "Returned external IP: 1.3.3.7"
        assert ext_ip_out["sensorid"] == "1234"
        assert {
            "name": "Response Time",
            "mode": "float",
            "value": 2,
            "kind": "TimeResponse",
        } in ext_ip_out["channel"]

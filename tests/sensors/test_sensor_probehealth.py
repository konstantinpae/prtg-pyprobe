import asyncio
import collections
import platform
import time

import psutil
import pytest
from psutil._common import sswap, sdiskpart

import prtg_pyprobe.sensors.sensor_probe_health as module
from prtg_pyprobe.sensors.sensor import SensorPSUtilBase


def task_data_probehealth(diskfull="1"):
    return {
        "sensorid": "1234",
        "diskfull": diskfull,
        "host": "127.0.0.1",
        "kind": "mpprobehealth",
        "celfar": "1",
    }


def uname_res():
    uname_result_mock = collections.namedtuple("uname_result", "system node release version machine processor")
    return uname_result_mock(
        system="Darwin",
        node="NUE-MAC-065",
        release="19.5.0",
        version="Darwin Kernel Version 19.5.0: Tue May 26 20:41:44 PDT 2020; root:xnu-6153.121.2~2/RELEASE_X86_64",
        machine="x86_64",
        processor="i386",
    )


def part_disk_usage(*args, **kwargs):
    sdiskusage_mock = collections.namedtuple("sdiskusage", ["total", "used", "free", "percent"])
    return sdiskusage_mock(total=250685575168, used=11003236352, free=65329041408, percent=14.4)


def cpu_load():
    return 3.39453125, 2.99853515625, 2.93310546875


def vmem():
    svmem_mock = collections.namedtuple(
        "svmem",
        [
            "total",
            "available",
            "percent",
            "used",
            "free",
            "active",
            "inactive",
            "wired",
        ],
    )
    return svmem_mock(
        total=17179869184,
        available=5667594240,
        percent=67.0,
        used=9245196288,
        free=263774208,
        active=5406777344,
        inactive=5364199424,
        wired=3838418944,
    )


def swapmem():
    return sswap(
        total=2147483648,
        used=1039663104,
        free=1107820544,
        percent=48.4,
        sin=21029543936,
        sout=162045952,
    )


def disk_part():
    return [
        sdiskpart(
            device="/dev/disk1s5",
            mountpoint="/",
            fstype="apfs",
            opts="ro,local,rootfs,dovolfs,journaled,multilabel",
        ),
        sdiskpart(
            device="/dev/disk1s1",
            mountpoint="/System/Volumes/Data",
            fstype="apfs",
            opts="rw,local,dovolfs,dontbrowse,journaled,multilabel",
        ),
        sdiskpart(
            device="/dev/disk1s4",
            mountpoint="/private/var/vm",
            fstype="apfs",
            opts="rw,local,dovolfs,dontbrowse,journaled,multilabel",
        ),
    ]


def psutil_error():
    raise psutil.Error


def psutil_boottime():
    return 5.0


def time_now():
    return 17.0


class TestProbeHealthSensorProperties:
    def test_sensor_probehealth_base_class(self, probehealth_sensor):
        assert type(probehealth_sensor).__bases__[0] is SensorPSUtilBase

    def test_sensor_probehealth_name(self, probehealth_sensor):
        assert probehealth_sensor.name == "Probe Health"

    def test_sensor_probehealth_kind(self, probehealth_sensor):
        assert probehealth_sensor.kind == "mpprobehealth"

    def test_sensor_probehealth_definition(self, probehealth_sensor):
        assert probehealth_sensor.definition == {
            "default": "yes",
            "description": "Sensor used to monitor the health of the system on which the " "python probe is runnning",
            "groups": [
                {
                    "caption": "Temperature Settings",
                    "fields": [
                        {
                            "caption": "Choose between Celsius or Fahrenheit " "display",
                            "default": "1",
                            "help": "Choose whether you want to return the value "
                            "in Celsius or Fahrenheit. When changed after "
                            "sensor is created, the unit will disappear "
                            "in the web interface.",
                            "name": "celfar",
                            "options": {"1": "Celsius", "2": "Fahrenheit"},
                            "type": "radio",
                        }
                    ],
                    "name": "temperature",
                },
                {
                    "caption": "Disk Space Settings",
                    "fields": [
                        {
                            "caption": "Full Display of all disk space values",
                            "default": "1",
                            "help": "Choose wether you want to get all disk space " "data or only percentages.",
                            "name": "diskfull",
                            "options": {"1": "Percentages", "2": "Full Information"},
                            "type": "radio",
                        }
                    ],
                    "name": "diskspace",
                },
            ],
            "help": "Sensor used to monitor the health of the system on which the python " "probe is runnning",
            "kind": "mpprobehealth",
            "name": "Probe Health",
            "tag": "mpprobehealthsensor",
        }


@pytest.mark.asyncio
class TestProbeHealthSensorWork:
    async def test_sensor_probehealth_work_success_disk_basic(
        self, probehealth_sensor, monkeypatch, psutil_system_temperatures, mocker
    ):
        monkeypatch.setattr(platform, "uname", uname_res)
        monkeypatch.setattr(psutil, "getloadavg", cpu_load)
        monkeypatch.setattr(psutil, "virtual_memory", vmem)
        monkeypatch.setattr(psutil, "swap_memory", swapmem)
        monkeypatch.setattr(psutil, "disk_partitions", disk_part)
        monkeypatch.setattr(psutil, "disk_usage", part_disk_usage)
        monkeypatch.setattr(psutil, "boot_time", psutil_boottime)
        monkeypatch.setattr(time, "time", time_now)
        psutil_mock = mocker.patch.object(probehealth_sensor, "get_system_temperatures")
        psutil_mock.return_value = psutil_system_temperatures

        probehealth_queue = asyncio.Queue()
        await probehealth_sensor.work(task_data=task_data_probehealth(), q=probehealth_queue)
        out = await probehealth_queue.get()
        assert out == {
            "channel": [
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "acpitz",
                    "unit": "Custom",
                    "value": 60.5,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Package id 0",
                    "unit": "Custom",
                    "value": 53.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Core 0",
                    "unit": "Custom",
                    "value": 53.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Core 1",
                    "unit": "Custom",
                    "value": 52.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Package id 0",
                    "unit": "Custom",
                    "value": 53.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Core 0",
                    "unit": "Custom",
                    "value": 53.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Core 1",
                    "unit": "Custom",
                    "value": 52.0,
                },
                {"customunit": "C", "kind": "Custom", "mode": "float", "name": "CPU", "unit": "Custom", "value": 60.0},
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Ambient",
                    "unit": "Custom",
                    "value": 39.0,
                },
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Ambient",
                    "unit": "Custom",
                    "value": 32.0,
                },
                {"customunit": "C", "kind": "Custom", "mode": "float", "name": "GPU", "unit": "Custom", "value": 16.0},
                {
                    "customunit": "C",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Other",
                    "unit": "Custom",
                    "value": 43.0,
                },
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /", "value": 14.4},
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /System/Volumes/Data", "value": 14.4},
                {"kind": "Percent", "mode": "float", "name": "Disk Space Percent /private/var/vm", "value": 14.4},
                {"kind": "BytesMemory", "mode": "integer", "name": "Memory Total", "value": 17179869184},
                {"kind": "BytesMemory", "mode": "integer", "name": "Memory Available", "value": 5667594240},
                {"kind": "BytesMemory", "mode": "integer", "name": "Swap Total", "value": 2147483648},
                {"kind": "BytesMemory", "mode": "integer", "name": "Swap Free", "value": 1107820544},
                {
                    "customunit": "",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Load average 1 min",
                    "value": 3.39453125,
                },
                {
                    "customunit": "",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Load average 5 min",
                    "value": 2.99853515625,
                },
                {
                    "customunit": "",
                    "kind": "Custom",
                    "mode": "float",
                    "name": "Load average 10 min",
                    "value": 2.93310546875,
                },
                {"kind": "TimeSeconds", "mode": "float", "name": "System Uptime", "value": 12.0},
            ],
            "message": "Running on Darwin in version Darwin Kernel Version 19.5.0: Tue "
            "May 26 20:41:44 PDT 2020; root:xnu-6153.121.2~2/RELEASE_X86_64! "
            "OK",
            "sensorid": "1234",
        }

    async def test_sensor_probehealth_work_success_disk_full(self, probehealth_sensor, monkeypatch):
        monkeypatch.setattr(platform, "uname", uname_res)
        monkeypatch.setattr(psutil, "getloadavg", cpu_load)
        monkeypatch.setattr(psutil, "virtual_memory", vmem)
        monkeypatch.setattr(psutil, "swap_memory", swapmem)
        monkeypatch.setattr(psutil, "disk_partitions", disk_part)
        monkeypatch.setattr(psutil, "disk_usage", part_disk_usage)
        monkeypatch.setattr(psutil, "boot_time", psutil_boottime)
        monkeypatch.setattr(time, "time", time_now)
        probehealth_queue = asyncio.Queue()
        await probehealth_sensor.work(task_data=task_data_probehealth(diskfull="2"), q=probehealth_queue)
        out = await probehealth_queue.get()
        assert out == {
            "sensorid": "1234",
            "message": "Running on Darwin in version Darwin Kernel Version 19.5.0: Tue May 26 20:41:44 PDT 2020; "
            "root:xnu-6153.121.2~2/RELEASE_X86_64! OK",
            "channel": [
                {
                    "name": "Disk Space Percent /",
                    "mode": "float",
                    "value": 14.4,
                    "kind": "Percent",
                },
                {
                    "name": "Disk Space Total /",
                    "mode": "integer",
                    "value": 250685575168,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Disk Space Used /",
                    "mode": "integer",
                    "value": 11003236352,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Disk Space Free /",
                    "mode": "integer",
                    "value": 65329041408,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Disk Space Percent /System/Volumes/Data",
                    "mode": "float",
                    "value": 14.4,
                    "kind": "Percent",
                },
                {
                    "name": "Disk Space Total /System/Volumes/Data",
                    "mode": "integer",
                    "value": 250685575168,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Disk Space Used /System/Volumes/Data",
                    "mode": "integer",
                    "value": 11003236352,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Disk Space Free /System/Volumes/Data",
                    "mode": "integer",
                    "value": 65329041408,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Disk Space Percent /private/var/vm",
                    "mode": "float",
                    "value": 14.4,
                    "kind": "Percent",
                },
                {
                    "name": "Disk Space Total /private/var/vm",
                    "mode": "integer",
                    "value": 250685575168,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Disk Space Used /private/var/vm",
                    "mode": "integer",
                    "value": 11003236352,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Disk Space Free /private/var/vm",
                    "mode": "integer",
                    "value": 65329041408,
                    "kind": "BytesDisk",
                },
                {
                    "name": "Memory Total",
                    "mode": "integer",
                    "value": 17179869184,
                    "kind": "BytesMemory",
                },
                {
                    "name": "Memory Available",
                    "mode": "integer",
                    "value": 5667594240,
                    "kind": "BytesMemory",
                },
                {
                    "name": "Swap Total",
                    "mode": "integer",
                    "value": 2147483648,
                    "kind": "BytesMemory",
                },
                {
                    "name": "Swap Free",
                    "mode": "integer",
                    "value": 1107820544,
                    "kind": "BytesMemory",
                },
                {
                    "name": "Load average 1 min",
                    "mode": "float",
                    "value": 3.39453125,
                    "kind": "Custom",
                    "customunit": "",
                },
                {
                    "name": "Load average 5 min",
                    "mode": "float",
                    "value": 2.99853515625,
                    "kind": "Custom",
                    "customunit": "",
                },
                {
                    "name": "Load average 10 min",
                    "mode": "float",
                    "value": 2.93310546875,
                    "kind": "Custom",
                    "customunit": "",
                },
                {"name": "System Uptime", "mode": "float", "value": 12, "kind": "TimeSeconds"},
            ],
        }

    async def test_sensor_probehealth_psutil_exception(
        self, probehealth_sensor, monkeypatch, sensor_exception_message, mocker
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(psutil, "getloadavg", psutil_error)
        probehealth_queue = asyncio.Queue()
        await probehealth_sensor.work(task_data=task_data_probehealth(), q=probehealth_queue)
        out = await probehealth_queue.get()
        sensor_exception_message["message"] = "Probehealth check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("Probe health information could not be determined")
        logging_mock.exception.assert_called_once()

from setuptools import setup

setup(
    name="prtg_pyprobe",
    description="Python Probe for PRTG Network Monitor",
    package_data={"": ["*.service"]},
    install_requires=[
        "click",
        "requests",
        "PyInquirer",
        "pythonping",
        "pyyaml",
        "certifi",
        "cerberus",
        "psutil",
        "aiodns",
        "cchardet",
        "dnspython",
        "pysnmp",
        "aioping",
        "httpx",
        "python-daemon",
        "backoff",
        "aioboto3",
        "dpath",
        "prometheus_client",
    ],
    tests_require=[
        "pytest",
        "pytest-cov",
        "pytest-asyncio",
        "pytest-mock",
        "pytest-subprocess",
        "black",
        "pyfakefs",
        "testfixtures",
        "flake8",
        "asynctest",
    ],
)
